#include <cstdlib>
#include "Server.h"
#include <iostream>

int main(int argc, char** argv)
{
	int port = 27015;
	if (argc == 2) {
		port = atoi(argv[1]);
	}
	Server server(port, 1024);
	if (!server.isErrorHappend) {
		server.Run();
		string command;
		while (!server.isErrorHappend) {
			cin >> command;
			if (command.compare("exit") == 0) {
				server.Stop();
				server.Exit();
				return 0;
			}
		}
		server.Stop();
		server.Exit();
	}
    return 0;
}
