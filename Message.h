#pragma once
#include <string>
#include <arpa/inet.h>

using namespace std;

/* Класс для сохранения всей информации о сообщениях и определения типов сообщений. */
class Message {
public:
	Message();
	Message(const Message& other);
	Message(string text);
	Message(string text, sockaddr_in* addr, int status);
	Message(string text, sockaddr_in* addr, string sAddress, int status);
	virtual ~Message();

	void SetText(string text) { this->text = text; }
	string GetText() { return text; }
	void SetAddress(sockaddr_in* addr);
	void SetAddress(sockaddr_in* addr, string sAddress);
	string GetSAddress() { return sAddress; }
	const sockaddr_in* const GetAddress() { return address; }
	void SetStatus(int status) { this->status = status; }
	int GetStatus() { return status; }

	bool IsMessageFrom(string address) { return sAddress.compare(address) == 0; };
	bool IsTextEqual(string text) { return this->text.compare(text) == 0; };
	bool IsItServiceMessage() { return this->text[0] != '@'; };
	bool IsMessageStartingWith(string text) { return this->text.find(text) == 0; };

private:

public:
	static int i;
private:
	string text;
	string sAddress;             /* строка адреса отправителя/получателя в виде ip:port */
	struct sockaddr_in* address; /* вся информация об адресе отправителе */
	int status;                  /* индекс, показывающий, что нужно сделать с сообщением. Описание см. в Utils.h */
};
