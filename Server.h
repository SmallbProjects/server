#pragma once
#include <queue>
#include <string>
#include "Message.h"
#include <vector>

using namespace std;

struct client {
	string sAddress;   /* строка адреса в виде ip:port */
	string nick;
	string status;
	bool isPongNeeded;
	struct sockaddr_in* address;
};

/* Класс содержащий базовую информацию о сервере и набор
 * методов для его работы */
class Server {
public:
	Server(int port, int bufSize);
	virtual ~Server();

	void Run();
	void Stop();
	void Exit();
private:
	void InitializeSocket();
	void SetupListeningSocket();

	sockaddr_in* createServerAddress();

	pthread_t RunThread(void *(*function)(void*));

	static void * RunNetworking(void *arg);
	void Network();
	Message ReceiveMessage();
	void SendAllMessages();
	void SendMessage(Message msg);
	void SendAll(Message msg);
	void SendExcept(Message msg);
	void SendTo(Message msg);

	static void * RunProcessing(void *arg);
	void Process();
	void KeepConnectionWithAllClients();
	void ProcessMessage(Message * msg);
	void ProcessServiceMessage(client* client, Message* msg);
	void ProcessClientMessage(client* client, Message * msg);
	void SendCommandToReceivingThread(string text);
	void PrepareForPingAndDefineOfflineClients();
	void ChangeClientStatus(client* client, string status, Message* msg);
	void RegisterNewClient(Message * msg);
	void ReRegisterClient(client * client, Message * processingMessage);
	string GenerateClientsList();
	void Log(string Message);

	int GetClientIndex(string sender);
	string GetNickFromRegisterCommand(string command);
public:
	bool isServerRunning;
	bool isErrorHappend;
private:
	int port;

	struct sockaddr_in* serverAddress;
	string serverSAddress; /* строка адреса сервера в виде ip:port */
	unsigned int sockaddr_inSize;

	int listeningSocketDescriptor;

	int bufSize;
	char * buffer;

	queue<Message> receivedMessages; // Очередь, в которую передаются сообщения для обработки
	queue<Message> messagesToSend;   // Очередь, в которую передаются сообщения для пересылки

	vector<client> clients;

	/* Чтобы не прерывать процесс обработки сообщений командой usleep,
	 * usleep вызывается это кол-во раз на меньшее время */
	int iterationsForNextPingTest;
};
