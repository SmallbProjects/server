#pragma once

/* файл с различным константами */

enum msgStatuses {
	MESSAGE_RECEIVED     /* RECEIVED - сообщение получено */
	, MESSAGE_SENDTO     /* SENDTO - отправить клиенту с адресом address */
	, MESSAGE_SENDEXCEPT /* SENDEXCEPT - отправить всем клиентам онлайн, кроме клиента с адресом address */
	, MESSAGE_SENDALL    /* SENDALL - отправить всем клиентам онлайн */
};

/* Служебное сообщение, посылаемый обрабатывающим потоком для посылки ping пакетов потоком, работающим с сетью */
const string SERVICE_MESSAGE_PING = "ping";
/* Служебное сообщение, посылаемой обрабатывающим потоком для прерывания получения сообщений потоком, работающим с сетью */
const string SERVICE_MESSAGE_INTERRUPT = "interrupt";

/* Различные статусы клиента */
const string CLIENT_ONLINE = "online";
const string CLIENT_OFFLINE = "offline";
const string CLIENT_REGISTERED = "registered";
const string CLIENT_REGISTERING = "register";
