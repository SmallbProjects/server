#include <string>
#include <sstream>
#include <arpa/inet.h>
using namespace std;

/* файл с функциями "общего" назначения (не имеют прямого отношения к существующим классам) */

static string IntToString(int i) {
	std::string s;
	std::stringstream out;
	out << i;
	s = out.str();
	return s;
}

static string AddressToString(sockaddr_in* addr) {
	return string(inet_ntoa(addr->sin_addr)) + ":" + IntToString(ntohs(addr->sin_port));
}




