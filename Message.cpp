#include "Message.h"
#include "Utils/Functions.cpp"
#include <string>
#include "Utils/Utils.h"

Message::Message() {
	this->address = 0;
	this->status = 0;
	this->text = "";
	this->sAddress = "";
}

Message::Message(const Message& other) {
	this->text = other.text;
	if (other.address != 0) {
		this->address = new sockaddr_in;
		*this->address = *other.address;
	}
	else {
		this->address = 0;
	}
	this->sAddress = other.sAddress;
	this->status = other.status;
}

Message::Message(string text) {
	this->address = 0;
	this->status = 0;
	this->text = text;
	this->sAddress = "";
}

Message::Message(string text, sockaddr_in* addr, int status) {
	this->text = text;
	if (addr != 0) {
		this->address = new sockaddr_in;
		*this->address = *addr;
	}
	else {
		this->address = 0;
	}
	this->sAddress = AddressToString(addr);
	this->status = status;
}

Message::Message(string text, sockaddr_in* addr, string sAddress, int status) {
	this->text = text;
	if (addr != 0) {
		this->address = new sockaddr_in;
		*this->address = *addr;
	}
	else {
		this->address = 0;
	}
	this->sAddress = sAddress;
	this->status = status;
}

Message::~Message() {
	delete address;
}

void Message::SetAddress(sockaddr_in* addr) {
	delete address;
	if (addr != 0) {
		this->address = new sockaddr_in;
		*this->address = *addr;
		this->sAddress = AddressToString(addr);
	}
	else {
		this->address = 0;
	}
}

void Message::SetAddress(sockaddr_in* addr, string sAddress) {
	delete address;
	if (addr != 0) {
		this->address = new sockaddr_in;
		*this->address = *addr;
	}
	else {
		this->address = 0;
	}
	this->sAddress = sAddress;
}
