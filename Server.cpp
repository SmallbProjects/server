#include "Server.h"
#include "Utils/Utils.h"
#include "Utils/Functions.cpp"

#include <cstdio>
#include <iostream>
#include <string.h>
#include <syslog.h>

#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

Server::Server(int port, int bufSize) {
	this->port = port;

	this->serverAddress = createServerAddress();
	this->serverSAddress = "127.0.0.1:" + IntToString(ntohs(serverAddress->sin_port));
	this->sockaddr_inSize = sizeof(*this->serverAddress);

	this->isServerRunning = false;
	this->isErrorHappend = false;

	this->bufSize = bufSize;
	this->buffer = new char[bufSize];

	this->iterationsForNextPingTest = 99;

	this->listeningSocketDescriptor = 0;
    openlog("Server",LOG_PID,LOG_USER);

	try {
		InitializeSocket();
		SetupListeningSocket();
	}
	catch(const runtime_error &e) {
		cout << e.what() << endl;
		isErrorHappend = true;
	}
}

sockaddr_in* Server::createServerAddress() {
	sockaddr_in* result = new sockaddr_in;
	memset(result, 0, sizeof(*result));
	result->sin_family = AF_INET;
	result->sin_addr.s_addr = htonl(INADDR_ANY);
	result->sin_port = htons(port);
	return result;
}

Server::~Server() {
	delete [] buffer;
	delete serverAddress;
	vector<client>::iterator it = clients.begin();
	while (it != clients.end()) {
		delete it->address;
		++it;
	}
}

/* Связь будет происходить через udp пакеты, т.к. через tcp соединение
 * в два потока не уложиться (нужен как минимум один поток для каждого клиента) */
void Server::InitializeSocket() {
	listeningSocketDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
	if (listeningSocketDescriptor < 0) {
		throw runtime_error("Ошибка при инициализации сокета.");
	}
}

void Server::SetupListeningSocket() {
	bool isBindingFailed = bind(listeningSocketDescriptor, (struct sockaddr*)serverAddress, sockaddr_inSize) < 0;
	if (isBindingFailed) {
		throw runtime_error("Ошибка при привязывания адреса. Адрес уже занят.");
	}
}

/* Запускает обрабатывающий и работающий с сетью потоки */
void Server::Run() {
	pthread_t processingThread = RunThread(RunProcessing);
	pthread_t receivingThread = RunThread(RunNetworking);
	if (!isErrorHappend) {
		isServerRunning = true;
		cout << "Сервер ожидает клиентов на порту " << port << ". Введите exit для выхода." << endl;
	}
}

pthread_t Server::RunThread(void* (*function)(void*)) {
	pthread_t thread;
	int result = pthread_create(&thread, NULL, function, this);
	if (result != 0) {
		cout << "Ошибка при создании потока" << endl;
		isErrorHappend = true;
		return 0;
	}
	return thread;
}

void * Server::RunNetworking(void * arg) {
	Server *s = (Server *)arg;
	s->Network();
	return 0;
}

/* Поток, работающий с сетью
 * Поток отправляет все сообщения от обрабатывающего потока.
 * Придти к нему могут или сообщения от обрабатывабщего потока (isMessageFromServer = true)
 * или от клиентов (isMessageFromServer = false)*/
void Server::Network() {
	while (!isServerRunning)
		usleep(100000);
	while (isServerRunning && !isErrorHappend) {

		while (!messagesToSend.empty()) {
			SendAllMessages();
		}

		Message receivedMessage = ReceiveMessage();

		bool isMessageFromServer = receivedMessage.IsMessageFrom(serverSAddress);
		if (isMessageFromServer) {
			bool isItPingTime = receivedMessage.IsTextEqual(SERVICE_MESSAGE_PING);
			if (isItPingTime) {
				SendAll(receivedMessage);
			}
			bool isItInterruptMessage = receivedMessage.IsTextEqual(SERVICE_MESSAGE_INTERRUPT);
			if (isItInterruptMessage){
				continue;
			}
		} else {
			receivedMessages.push(receivedMessage);
		}
	}
}

void Server::SendAllMessages() {
	Message messageToSend = messagesToSend.front();
	messagesToSend.pop();
	SendMessage(messageToSend);
}

Message Server::ReceiveMessage() {
	sockaddr_in currentClientAddr;
	memset(&currentClientAddr, 0, sizeof(currentClientAddr));

	int bytesReceived = recvfrom(listeningSocketDescriptor, buffer, bufSize, 0, (struct sockaddr *)&currentClientAddr, &sockaddr_inSize);
	string text = string(buffer, bytesReceived);
	Message receivedMessage(text, &currentClientAddr, MESSAGE_RECEIVED);
	return receivedMessage;
}

void Server::SendMessage(Message msg) {
	switch (msg.GetStatus()) {
		case(MESSAGE_SENDALL):
				SendAll(msg);
			break;
		case(MESSAGE_SENDEXCEPT):
				SendExcept(msg);
			break;
		case(MESSAGE_SENDTO):
				SendTo(msg);
			break;
	}
}

void Server::SendAll(Message msg) {
	vector<client>::iterator it = clients.begin();
	while (it != clients.end()) {
		if ((*it).status.compare(CLIENT_ONLINE) == 0 || (*it).isPongNeeded) {
			msg.SetAddress((*it).address, (*it).sAddress);
			SendTo(msg);
		}
		++it;
	}
}

void Server::SendExcept(Message msg) {
	string except = msg.GetSAddress();
	vector<client>::iterator it = clients.begin();
	while (it != clients.end()) {
		if ((*it).status.compare(CLIENT_ONLINE) == 0 || (*it).isPongNeeded) {
			if (it->sAddress.compare(except) != 0) {
				msg.SetAddress((*it).address, (*it).sAddress);
				SendTo(msg);
			}
		}
		++it;
	}
}

void Server::SendTo(Message msg) {
	int textSize = msg.GetText().size();
	if (textSize < 1024) {
		if (sendto(listeningSocketDescriptor, msg.GetText().c_str(), textSize, 0, (struct sockaddr *)msg.GetAddress(), sockaddr_inSize)==-1)
			cout << "Произошла ошибка при отправке сообщения клиенту с адресом: " << msg.GetSAddress() << endl;
	}
	else {
		cout << "Не удалось отправить сообщение. Сообщение слишком длинное." << endl;
	}
}

void * Server::RunProcessing(void *arg) {
	Server *s = (Server *)arg;
	s->Process();
	return 0;
}

/* Поток, который проверяет связь с клиентами и обрабатывает сообщения */
void Server::Process() {
	while (!isServerRunning)
		usleep(100000);
	while(isServerRunning && !isErrorHappend) {
		KeepConnectionWithAllClients();
		while(!receivedMessages.empty()) {
			Message processingMessage = receivedMessages.front();
			receivedMessages.pop();

			ProcessMessage(&processingMessage);
		}
	}
}

/* Проверяет связь с клиентами каждые iterationsForNextPingTest*10000/1000000 секунд */
void Server::KeepConnectionWithAllClients() {
	usleep(10000);
	iterationsForNextPingTest--;
	bool isItPingTime = iterationsForNextPingTest == 0;
	if (isItPingTime) {
		iterationsForNextPingTest = 49;
		PrepareForPingAndDefineOfflineClients();
		SendCommandToReceivingThread("ping");
	}
}

void Server::PrepareForPingAndDefineOfflineClients() {
	vector<client>::iterator it = clients.begin();
	while (it != clients.end()) {
		if ((*it).status.compare(CLIENT_ONLINE) == 0) {
			(*it).status = CLIENT_OFFLINE;
			(*it).isPongNeeded = true;
		}
		else {
			bool isClientAnsweredOnPing = !(*it).isPongNeeded;
			if (!isClientAnsweredOnPing) {
				Message msg;
				ChangeClientStatus(&(*it), CLIENT_OFFLINE, &msg);
				messagesToSend.push(msg);
			}
		}
		++it;
	}
}

/*Сообщения делятся на два типа: служебные (просто команда)
 *и не служебные (начинаются с @, текст от клиентов)*/
void Server::ProcessMessage(Message * msg) {
	int clientIndex = GetClientIndex(msg->GetSAddress());
	bool isClientNew = clientIndex == -1;
	if (isClientNew) {
		bool isClientRegistering = msg->IsMessageStartingWith(CLIENT_REGISTERING);
		if  (isClientRegistering) {
			RegisterNewClient(msg);
		}
	}
	else {
		struct client* currentClient = &clients.at(clientIndex);
		if (msg->IsItServiceMessage()) {
			ProcessServiceMessage(currentClient, msg);
		}
		else {
			ProcessClientMessage(currentClient, msg);
		}
	}

	if (msg->GetStatus() != MESSAGE_RECEIVED) {
		messagesToSend.push(*msg);
		SendCommandToReceivingThread("interrupt");
	}
}

/* Общение обрабатывающего потока с другим с помощью сокета */
void Server::SendCommandToReceivingThread(string text) {
	Message result(text, serverAddress, serverSAddress, MESSAGE_SENDTO);
	SendTo(result);
}

/* Возвращает индекс клиента или -1 если клиент отсутствует */
int Server::GetClientIndex(string senderIP) {
	vector<client>::iterator it = clients.begin();
	while (it != clients.end()) {
		if ((*it).sAddress.compare(senderIP) == 0) {
			return it - clients.begin();
		}
		++it;
	}
	return -1;
}

/* Сообщения или "pong" (ответ от клиентов на ping) или "register" (перерегистрация для смены ника) */
void Server::ProcessServiceMessage(client* client, Message * msg) {
	if (msg->IsTextEqual("pong")) {
		client->isPongNeeded = false;
		client->status = CLIENT_ONLINE;
	}
	else {
		bool isClientReRegistring = msg->IsMessageStartingWith(CLIENT_REGISTERING);
		if  (isClientReRegistring) {
			ReRegisterClient(client, msg);
		}
		else {
			Log("Неизвестная команда \"" + msg->GetText() + "\". Где-то закралась ошибка..");
		}
	}
}

void Server::RegisterNewClient(Message * msg) {
	struct client newClient;
	newClient.sAddress = msg->GetSAddress();
	newClient.address = new sockaddr_in;
	*newClient.address = *msg->GetAddress();

	ReRegisterClient(&newClient, msg);

	clients.push_back(newClient);
}

void Server::ReRegisterClient(client * client, Message * msg) {
	client->nick = GetNickFromRegisterCommand(msg->GetText());

	ChangeClientStatus(client, CLIENT_REGISTERED, msg);

	Message registeredWithAddress(
			"Вы зарегестрированы с адресом " + client->sAddress + "."
			, client->address
			, MESSAGE_SENDTO
	);
	messagesToSend.push(registeredWithAddress);
}

void Server::ProcessClientMessage(client* client, Message * msg) {
	if (msg->IsMessageStartingWith("@list")) {
		msg->SetText(GenerateClientsList());
		msg->SetStatus(MESSAGE_SENDTO);
	}
	else {
		if (msg->IsMessageStartingWith("@bye")) {
			ChangeClientStatus(client, CLIENT_OFFLINE, msg);
		}
		else {
			msg->SetStatus(MESSAGE_SENDEXCEPT);
		}
	}
}

string Server::GenerateClientsList() {
	string result = "@";
	vector<client>::iterator it = clients.begin();
	while (it != clients.end()) {
		string status = ((*it).isPongNeeded || (*it).status.compare(CLIENT_ONLINE) == 0) ? CLIENT_ONLINE : CLIENT_OFFLINE;
		result += (*it).sAddress + " " + (*it).nick + " " + status + "\n";
		++it;
	}
	result = result.substr(0, result.length() - 1);
	return result;
}

void Server::ChangeClientStatus(client* client, string status, Message* msg) {
	string messageText = "Клиент " + client->nick + " с адресом " + client->sAddress + " " + status + ".";
	Log(messageText);
	client->isPongNeeded = false;
	if (status.compare(CLIENT_REGISTERED) == 0) {
		client->status = CLIENT_ONLINE;
		msg->SetStatus(MESSAGE_SENDEXCEPT);
	}
	else {
		client->status = status;
		msg->SetStatus(MESSAGE_SENDALL);
	}
	msg->SetText("@" + messageText);
}

string Server::GetNickFromRegisterCommand(string command) {
	return command.substr(command.find(' ') + 1);
}

void Server::Log(string text) {
	cout << text << endl;
	syslog(LOG_INFO, "%s\n", text.c_str());
}

void Server::Stop() {
	if (isServerRunning) {
		SendCommandToReceivingThread(SERVICE_MESSAGE_INTERRUPT);
		isServerRunning = false;
	}
}

void Server::Exit() {
	close(listeningSocketDescriptor);
	closelog();
}
